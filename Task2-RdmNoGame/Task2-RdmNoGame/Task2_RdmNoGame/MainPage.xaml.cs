﻿using System;
using Xamarin.Forms;

namespace Task2_RdmNoGame
{
    public partial class MainPage : ContentPage 
    {
        public static int score = 0;
        
        public MainPage()
        {
            InitializeComponent();

            RNGame.GenerateNumber(5);
        }
             
        public void SubmitNo_Click(object sender, EventArgs e)
        {            
        }

        public void Calculate_Click(object sender, EventArgs e)
        {
            int intGeneratedRandom = RNGame.GenerateNumber(5);
            Roll.Text = intGeneratedRandom.ToString();
            
            int Input = int.Parse(UserInput.Text);
                                       
                if (Input == intGeneratedRandom)
                {
                    score = score + 1;
                    Message.Text = "You WIN!";                                                            
                    Background.BackgroundColor = Color.FromHex("#FF0");
            }
                else if (Input != intGeneratedRandom)  
                {                                   
                    Message.Text = "You LOOSE!";
                    Background.BackgroundColor = Color.White;
                }

        TotalWins.Text = score.ToString();
        }        
    }
}
