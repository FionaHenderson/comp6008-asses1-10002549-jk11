﻿
namespace Task1___MathProblem
{
    public class MathProblem
    {
        public static int Calculate(string _userNo)
        {
            var count = _userNo;
            int counter = int.Parse(count);
            var i = 0;
            var answer = 0;
        
            for (i = 0; i < counter; i++)
            {               
                if ((i % 3 == 0) || (i % 5 == 0))
                {
                    answer = answer + i;
                }
            }
            return answer;
        }      
    }
}
