﻿using System;
using Xamarin.Forms;

namespace Task1___MathProblem
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void ButtonClicked1(object sender, EventArgs e)
        {
            string result = UserInput.Text;
            Return.Text = MathProblem.Calculate(result).ToString();
        }
    }
}
